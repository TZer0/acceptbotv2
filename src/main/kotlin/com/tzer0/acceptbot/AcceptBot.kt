package com.tzer0.acceptbot

import discord4j.common.util.Snowflake
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.core.event.domain.message.MessageCreateEvent
import discord4j.rest.util.Permission
import discord4j.rest.util.PermissionSet
import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import java.sql.DriverManager


class AcceptBot(connection: BotConnection, val dbManager: DatabaseManager, dryRun : Boolean = false) {
    init {
        connection.gateway().on(MessageCreateEvent::class.java)
            .subscribe { event: MessageCreateEvent ->
                parseEvent(event)
            }
        if (!dryRun) {
            connection.gateway().onDisconnect().block()
        }
    }

    private fun parseEvent(event: MessageCreateEvent) {
        val command: List<String> = event.message.content.toLowerCase().split(' ')
        if (command.isEmpty()) {
            return;
        }
        if (event.member.isPresent) {
            val member: Member = event.member.get()
            if (command[0] == "aa" || command[0] == "acceptall") {
                if (!hasPermissions(member)) {
                    return;
                }

                val channel: MessageChannel = event.message.channel.block()
                val guildData: DatabaseManager.GuildData = dbManager.getGuildData(event.guildId.get().asLong())
                if (!guildData.isValid()) {
                    channel.createMessage(guildData.getErrorString()).block()
                    return;
                }
                val id = getIdOrReplyWithErrorMessage(command, channel) ?: return
                val members = ArrayList<Member>()

                val messageList = channel.getMessagesAfter(Snowflake.of(id-1)).collectList().block()
                messageList.forEach {
                    val author = it.authorAsMember.block()
                    if (!members.contains(author) && !author.isBot && author != member) {
                        if (it.content.toLowerCase().contains("accept")) {
                            members.add(it.authorAsMember.block())
                            it.addReaction(ReactionEmoji.unicode("✅")).block()
                        }
                    }
                }

                acceptMembers(members, guildData, event)

            } else if (command[0] == "a1" || command[0] == "acceptone") {
                if (!hasPermissions(member)) {
                    return;
                }
                val channel: MessageChannel = event.message.channel.block()
                val guildData: DatabaseManager.GuildData = dbManager.getGuildData(event.guildId.get().asLong())
                if (!guildData.isValid()) {
                    channel.createMessage(guildData.getErrorString()).block()
                    return;
                }
                val id = getIdOrReplyWithErrorMessage(command, channel) ?: return

                val member = event.guild.block().getMemberById(Snowflake.of(id)).blockOptional()
                if (member.isPresent) {
                    acceptMembers(listOf(member.get()), guildData, event)
                } else {
                    channel.createMessage("No member with that ID").block()
                }
            } else if (command[0] == "setmemberrole") {
                if (!hasPermissions(member)) {
                    return;
                }
                val channel: MessageChannel = event.message.channel.block()
                val id = getIdOrReplyWithErrorMessage(command, channel) ?: return

                dbManager.setAcceptRole(event.guildId.get().asLong(), id);
                channel.createMessage("Done!").block()
            } else if (command[0] == "setwelcomechannel") {
                if (!hasPermissions(member)) {
                    return;
                }
                val channel: MessageChannel = event.message.channel.block()
                dbManager.setWelcomeChannel(event.guildId.get().asLong(), event.message.channelId.asLong())
                channel.createMessage("Done!").block()
            } else if (command[0] == "!help") {
                if (!hasPermissions(member)) {
                    return;
                }

            }
        }
    }

    fun messagesMatches(message: String) : Boolean {
        val lowerMessage = message.toLowerCase()
        if (lowerMessage.contains("accept") ||
                lowerMessage.contains("rules") ||
                lowerMessage.contains("dang") ||
                lowerMessage.contains("agree")) {
            return true;
        }
        return false;
    }


    fun acceptMembers(members: List<Member>, guildData: DatabaseManager.GuildData, event: MessageCreateEvent) {
        var welcomeMessage = "";
        val roleSnowflake = Snowflake.of(guildData.acceptRoleId!!)
        val welcomeChannel = Snowflake.of(guildData.welcomeChannelId!!)

        members.forEach {
            if (!it.roles.collectList().block().any { it.id == roleSnowflake }) {
                it.addRole(roleSnowflake).block()
                if (welcomeMessage.isNotEmpty()) {
                    welcomeMessage += ", "
                }
                welcomeMessage += it.mention
            }
        }

        if (welcomeMessage == "") {
            event.message.channel.block().createMessage("No users accepted the rules.").block()
        } else {

            val messageChannel = event.guild.block().getChannelById(welcomeChannel).block()
            if (messageChannel is GuildMessageChannel) {
                val castChannel = messageChannel as GuildMessageChannel
                castChannel.createMessage {
                    it.setContent(welcomeMessage)
                    it.setEmbed {
                        it.setTitle("You're in!").addField("Information", "The following mod let you in: " + event.member.get().mention, true)
                    }

                }.block()
            }
        }
        event.message.delete().block()
    }

    fun getIdOrReplyWithErrorMessage(command: List<String>, channel: MessageChannel) : Long? {
        if (command.size != 2) {
            channel.createMessage("Command requires an argument").block()
            return null
        }

        val id = command[1].toLongOrNull()
        if (id == null) {
            channel.createMessage("Parameter must be a long").block()
            return null
        }

        return id
    }

    fun hasPermissions(member: Member) : Boolean {
        return hasPermissions(member.basePermissions.block())
    }

    fun hasPermissions(perms: PermissionSet) : Boolean {
        if (perms.contains(Permission.MANAGE_ROLES) || perms.contains(Permission.ADMINISTRATOR)) {
            return true
        }
        return false
    }

    object Main {
        @JvmStatic
        fun main(args: Array<String>) {
            val conn =  JdbcConnection( DriverManager.getConnection("jdbc:sqlite:database.sqlite"))

            Liquibase("db/changes.xml", ClassLoaderResourceAccessor(), conn).update(Contexts(), LabelExpression())
            AcceptBot(DiscordBotConnection(args[0]), DatabaseManager(conn))
        }
    }
}