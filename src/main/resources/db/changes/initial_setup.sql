--liquibase formatted sql

--changeset tzer0:initial

CREATE TABLE welcome_channel (
    guild_id BIGINT PRIMARY KEY,
    channel_id BIGINT NOT NULL
);

CREATE TABLE accept_role (
    guild_id BIGINT PRIMARY KEY,
    role_id BIGINT NOT NULL
);