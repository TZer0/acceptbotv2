package com.tzer0.acceptbot

import discord4j.core.DiscordClient
import discord4j.core.GatewayDiscordClient

public class DiscordBotConnection(token: String) : BotConnection {
    val discordClient = DiscordClient.create(token)
    val gatewayDiscordClient = discordClient.login().block()
    override fun client(): DiscordClient {
        return discordClient
    }

    override fun gateway(): GatewayDiscordClient {
        return gatewayDiscordClient
    }
}