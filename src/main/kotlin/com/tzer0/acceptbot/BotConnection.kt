package com.tzer0.acceptbot

import discord4j.core.DiscordClient
import discord4j.core.GatewayDiscordClient

interface BotConnection {
    fun client() : DiscordClient
    fun gateway() : GatewayDiscordClient
}