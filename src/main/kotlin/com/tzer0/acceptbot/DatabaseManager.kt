package com.tzer0.acceptbot

import liquibase.database.jvm.JdbcConnection
import javax.sql.DataSource

class DatabaseManager(connection : JdbcConnection) {
    val connection = connection
    fun setWelcomeChannel(guildId: Long, channelId: Long) {
        val statement = connection.prepareStatement("INSERT OR REPLACE INTO welcome_channel(guild_id, channel_id) VALUES(?, ?)")
        statement.setLong(1, guildId)
        statement.setLong(2, channelId)
        statement.execute()
        connection.commit()
        println(getWelcomeChannel(guildId));
    }

    fun getWelcomeChannel(guildId: Long) : Long? {
        val statement = connection.prepareStatement("SELECT channel_id FROM welcome_channel WHERE guild_id = ?")
        statement.setLong(1, guildId)
        statement.execute()
        val resultSet = statement.resultSet
        while (resultSet.next()) {
            return resultSet.getLong("channel_id")
        }
        return null
    }

    fun setAcceptRole(guildId: Long, memberRole: Long) {
        val statement = connection.prepareStatement("INSERT OR REPLACE INTO accept_role(guild_id, role_id) VALUES(?, ?)")
        statement.setLong(1, guildId)
        statement.setLong(2, memberRole)
        statement.execute()
        connection.commit()
    }

    fun getAcceptRole(guildId: Long) : Long? {
        val statement = connection.prepareStatement("SELECT role_id FROM accept_role WHERE guild_id = ?")
        statement.setLong(1, guildId)
        statement.execute()
        val resultSet = statement.resultSet
        while (resultSet.next()) {
            return resultSet.getLong("role_id")
        }
        return null
    }

    fun getGuildData(guildId: Long) : GuildData {
        val acceptRoleId = getAcceptRole(guildId);
        val welcomeChannelId = getWelcomeChannel(guildId)

        return GuildData(acceptRoleId, welcomeChannelId)
    }

    data class GuildData(val acceptRoleId: Long?, val welcomeChannelId: Long?) {
        fun isValid() : Boolean {
            return acceptRoleId != null && welcomeChannelId != null
        }

        fun getErrorString() : String? {
            if (acceptRoleId == null) {
                return "Missing accept role";
            }
            if (welcomeChannelId == null) {
                return "Missing welcome channel"
            }
            return null;
        }
    }
}